FROM cloudron/base:1.0.0@sha256:147a648a068a2e746644746bbfb42eb7a50d682437cead3c67c933c546357617

ENV DEBIAN_FRONTEND noninteractive
ENV NODE_ENV production

WORKDIR /app/code/current

RUN curl -L https://github.com/coralproject/talk/archive/master.tar.gz | tar -C /app/code/current --strip-components 1 -zxf -

ENV PATH /app/code/current/bin:$PATH

# talk really requires yarn!
RUN npm install -g yarn && \
    yarn global add node-gyp && \
    yarn install --frozen-lockfile && \
    yarn build && \
    yarn cache clean

#RUN rm -r /usr/local/share && ln -s /run/talk/share /usr/local/share

ADD start.sh /app/code/start.sh

EXPOSE 3000

CMD [ "/app/code/start.sh" ]
