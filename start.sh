#!/bin/bash
set -eu -o pipefail

# create data and logging directories
mkdir -p /run/talk /run/talk/cache /run/talk/home

echo ".. creating environment configs"
JWT_SECRET=$(pwgen -1s 32)

export NODE_ENV="production"
export NODE_PATH="/app/code/node_modules"

#export YARN_CACHE_FOLDER="/run/talk/cache"
#export YARN_CWD="/run/talk/code"

export TALK_MONGO_URL="mongodb://${MONGODB_USERNAME}:${MONGODB_PASSWORD}@${MONGODB_HOST}:${MONGODB_PORT}/${MONGODB_DATABASE}"
export TALK_REDIS_URL="redis://:${REDIS_PASSWORD}@${REDIS_HOST}:${REDIS_PORT}/0"
export TALK_ROOT_URL="${APP_ORIGIN}"
export TALK_PORT=3000
export TALK_JWT_SECRET="$JWT_SECRET"

#export HOME="/run/talk/home"

echo ".. starting talk"

yarn start
